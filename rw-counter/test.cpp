#include <gtest/gtest.h>
#include <util.h>

#include <rw_counter.h>

#include <vector>
#include <future>

TEST(Correctness, Basic) {
    ReadWriteAtomicCounter counter;
    std::vector<std::unique_ptr<std::thread>> writers;
    constexpr int kNumIncrements = 100;
    constexpr int kNumThreads = 10;
    for (int i = 0; i < kNumThreads; ++i) {
        writers.push_back(std::make_unique<std::thread>([&] {
            auto incrementer = counter.GetIncrementer();
            for (int i = 0; i < kNumIncrements; ++i) {
                incrementer->Increment();
            }
        }));
    }
    for (auto& thread : writers) {
        thread->join();
    }
    ASSERT_EQ(counter.GetValue(), kNumIncrements * kNumThreads);
}
